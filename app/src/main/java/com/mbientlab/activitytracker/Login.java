package com.mbientlab.activitytracker;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

/**
 * Created by User on 21/09/2016.
 */
public class Login extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void goToMain(View view) {
        startActivity(new Intent(Login.this,MainActivity.class));
        finish();
    }
}
